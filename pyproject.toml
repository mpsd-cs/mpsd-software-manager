[build-system]
requires = ["setuptools"]
build-backend = "setuptools.build_meta"

[project]
name = "mpsd_software_manager"
authors = [{name = "SSU-Computational Science (Fangohr et al)", email = "ssu-cs@mpsd.mpg.de"}]
license = {file = "LICENSE"}
classifiers = ["License :: OSI Approved :: MIT License"]
version = "2024.02.27"
readme = "README.rst"
requires-python = ">=3.9"
dependencies = [
    "archspec",
    "rich",
]
[project.scripts]
mpsd-software = "mpsd_software_manager.mpsd_software:main"

[project.urls]
homepage = "https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager/"
repository = "https://gitlab.gwdg.de/mpsd-cs/mpsd-software-manager/"

[project.optional-dependencies]
dev = [
    "pre-commit",
    "pytest",
    "pytest-mock",
    "pytest-cov",
]

[tool.ruff]
target-version = "py37"

[tool.ruff.lint]
select = [
    "B",   # flake8-bugbear
    "D",   # pydocstyle
    "E",   # pycodestyle
    "F",   # Pyflakes
    "I",   # isort
    "SIM", # flake8-simplify
    "UP",  # pyupgrade
]
ignore = [
    # conflict with formatter
    "D206",  # indent-with-spaces
    "D300",  # triple-single-quotes
    "E111",  # indentation-with-invalid-multiple
    "E114",  # indentation-with-invalid-multiple-comment
    "E117",  # over-indented
    # conflict with Python 3.6 compatibility
    "UP022", # replace-stdout-stderr
]

[tool.ruff.extend-per-file-ignores]
"tests/*" = ["D"]

[tool.ruff.lint.isort]
known-local-folder = ["mpsd_software_manager"]

[tool.ruff.lint.pydocstyle]
convention = "numpy"

[tool.pytest.ini_options]
addopts = [
    "--import-mode=importlib",
]
pythonpath = "src"
