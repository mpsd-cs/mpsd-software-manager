"""Functions to remove software."""
import logging
import os
import re
import sys

from mpsd_software_manager.utils.filesystem_utils import get_important_folders
from mpsd_software_manager.utils.logging import get_log_file_path
from mpsd_software_manager.utils.microarch import get_native_microarchitecture
from mpsd_software_manager.utils.run import run
from mpsd_software_manager.utils.spack_wrapper import spack_run


def remove_environment(mpsd_release, root_dir, package_sets="NONE", force_remove=False):
    """Remove release from installation.

    Handle 3 situations :
    1. remove does not specify what to remove
        -> warn and exit
    2. remove all package_sets from release
        -> remove release folder except logs
    3. remove specific package_sets from release
        -> remove spack environments via spack commands

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    root_dir : pathlib.Path
        A Path object representing the path to the directory where
        the release and package_sets will be installed.
    package_sets : list of str
        A list of strings representing the package_sets to remove
        (e.g., "foss2021a-mpi", "global_generic", "ALL").
    force_remove : bool, optional
        A boolean indicating whether to force remove the release.
        If False, the user will be prompted to confirm the removal.
        Defaults to False.

    Raises
    ------
    ValueError

    """
    msg = (
        f"Removing release {mpsd_release}"
        f" with package_sets {package_sets} from {root_dir}"
    )
    logging.warning(msg)

    if package_sets == "NONE":
        logging.warning(
            "Please specify package_sets to remove, or 'ALL' to remove all package_sets"
        )
        sys.exit(50)
    # 2nd case: remove the entire release for microarchitecture
    folders = get_important_folders(mpsd_release, root_dir)
    dir_to_remove = folders.package_set_dir
    if "ALL" in package_sets:
        # we need to remove the entire release folder
        logging.warning(
            f"Removing release {mpsd_release}"
            f" from {root_dir} for {get_native_microarchitecture()}"
        )
        if not force_remove:
            logging.warning("do you want to continue? [y/n]")
            if input().lower() != "y":
                sys.exit(60)

            # Set the remove log file name from create_log_file_names
        build_log_path = get_log_file_path(mpsd_release, "remove", root_dir, "ALL")

        logging.info(f"> Logging removal of {mpsd_release} at {build_log_path}")
        folders_to_remove = os.listdir(dir_to_remove)
        for folder in folders_to_remove:
            # shutil.rmtree(dir_to_remove / folder) #dosent delete file
            run(
                f"rm -rf {dir_to_remove / folder} 2>&1 | tee -a {build_log_path}",
                shell=True,
                check=True,
            )
        logging.warning(f"Removed release {mpsd_release} from {root_dir}")
        return
    # 3rd case: remove specific package_sets from release
    for package_set in package_sets:
        # we load the spack environment and remove the package_set
        build_log_path = get_log_file_path(
            mpsd_release, "remove", root_dir, package_set
        )
        logging.info(f"> Logging removal of {package_set} at {build_log_path}")
        if package_set not in ["global_generic", "global"]:
            remove_spack_environment(
                dir_to_remove / "spack", package_set, build_log_path
            )
        else:
            # list all specs from the global_packages.list
            spe_folder = root_dir / mpsd_release / "spack-environments"
            package_list_file = (
                spe_folder / "toolchains" / package_set / "global_packages.list"
            )
            with open(package_list_file) as f:
                package_dump = f.read()

            # remove all content from # to the end of the line
            package_dump = re.sub(r"#.*\n", "\n", package_dump)
            # replace \\n with "" to remove line breaks
            package_list = package_dump.replace("\\\n", "").split("\n")
            # remove all empty lines
            package_list = [line for line in package_list if line != ""]

            # remove all packages in package_list
            for package in package_list:
                logging.info(f"Removing package {package} from installation")
                remove_spack_package(dir_to_remove / "spack", package, build_log_path)


def remove_spack_environment(spack_dir, environment_name, build_log_path=None):
    """Remove spack environment including packages exclusive to it.

    First activate the environment,
    then uninstall all packages exclusive to the environment,
    then deactivate the environment,
    remove the environment,
    and finally remove the environment lua file.

    Parameters
    ----------
    spack_dir : pathlib.Path
        A Path object representing the path to the spack directory.
    environment_name : str
        A string representing the name of the spack environment to remove.
    build_log_path : pathlib.Path, optional
        A Path object representing the path to where the logs will be teed
    """
    logging.warning(f"Removing spack environment {environment_name}")
    long_spack_remove_bash_command = (
        f"spack env activate {environment_name} && "
        f"for spec in $(spack -e {environment_name} find"
        r' --format "{name}@{version}%{compiler.name}@{compiler.version}");do'
        " spack uninstall -y $spec; done && "
        "spack env deactivate"
    )
    build_log_path = build_log_path or "/dev/null"
    spack_run(spack_dir, long_spack_remove_bash_command, build_log_path)
    spack_run(spack_dir, f"spack env remove -y {environment_name}", build_log_path)

    # remove the environment lua file
    lua_file = (
        spack_dir / ".." / "lmod" / "Core" / "toolchains" / f"{environment_name}.lua"
    )
    run(f"rm {lua_file}", shell=True, check=True)


def remove_spack_package(spack_dir, package, build_log_path=None):
    """Remove spack package.

    Used to remove global packages.

    Parameters
    ----------
    spack_dir : pathlib.Path
        A Path object representing the path to the spack directory.
    package : str
        A string representing the name of the spack package to remove.
    build_log_path : pathlib.Path, optional
        A Path object representing the path to where the logs will be teed

    """
    logging.info(f"Removing spack package {package}")
    spack_run(spack_dir, f"spack uninstall -y {package}", build_log_path)
