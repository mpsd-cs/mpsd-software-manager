"""Functions to find available releases."""
import logging
import os
import re
import tempfile
from pathlib import Path
from typing import List

from mpsd_software_manager import config_vars
from mpsd_software_manager.utils.filesystem_utils import clone_repo


def get_available_package_sets(
    mpsd_release: str, allow_unreleased_branch: bool = False
) -> List[str]:
    """Given a release, return the available package_sets.

    This is based on the spack-environment's repository [1]. For this function
    to succeed, we need to have Internet access etc.

    We use a temporary directory to clone the repository locally, which is
    deleted upon successful completion of the function.

    [1] https://gitlab.gwdg.de/mpsd-cs/spack-environments.git

    Parameters
    ----------
    mpsd_release : str
        The release to check for available package_sets.
    allow_unreleased_branch : bool, optional
        If True, allow the use of unreleased branches. Defaults to False.


    Returns
    -------
    package_sets : List[str]

    Example
    -------
    >>> get_available_package_sets('dev-23a')
    ['foss2021a-cuda-mpi',
     'foss2021a-mpi',
     'foss2021a-serial',
     'foss2022a-cuda-mpi',
     'foss2022a-mpi',
     'foss2022a-serial',
     'global',
     'global_generic']

    """
    logging.debug(f"get_available_package_sets(mpsd_release={mpsd_release})")
    print_log = logging.getLogger("print")

    logging.info(f"Retrieving available package_sets for release {mpsd_release}")

    # create temporary directory
    tmp_dir = tempfile.TemporaryDirectory(prefix="mpsd-software-available-")
    tmp_dir_path = Path(tmp_dir.name)

    # find package_sets by cloning repository and checking out right branch
    spe_branch = mpsd_release if allow_unreleased_branch else f"releases/{mpsd_release}"

    clone_repo(
        tmp_dir_path,
        config_vars.spack_environments_repo,
        branch=spe_branch,
    )
    # look for directories defining the package_sets

    package_sets = os.listdir(tmp_dir_path / "toolchains")
    msg = f"Found package_sets {sorted(package_sets)}"
    logging.debug(msg)

    # the 'package_sets' split into toolchains (such as foss2022a-mpi) and sets
    # of packages. Here we split them into the two categories for a more useful
    # output:
    toolchain_list = [
        x.parents[0].name
        for x in list((tmp_dir_path / "toolchains").glob("*/spack.yaml"))
    ]
    package_set_list = [
        x.parents[0].name for x in list((tmp_dir_path / "toolchains").glob("*/*.list"))
    ]
    logging.debug(f"toolchain_list={toolchain_list}")
    logging.debug(f"package_set_list={package_set_list}")

    # summarise toolchains found for use, and show packages provided for each
    # package_set:
    print_log.info(
        f"MPSD software release {mpsd_release}, AVAILABLE for installation are"
    )
    print_log.info("Toolchains: \n    " + "\n    ".join(sorted(toolchain_list)))
    print_log.info("Package sets:")
    for package_set in package_set_list:
        # get a list of all packages which
        # starts from the first line of the file
        # that have the regex pattern \w+@\w+
        with open(
            tmp_dir_path / "toolchains" / package_set / "global_packages.list"
        ) as fh:
            packages = [
                line.split()[0].split("%")[0]
                for line in fh.readlines()
                if re.match(r"^\w+@\w+", line)
            ]
            print_log.info(f"    {package_set} ({', '.join(packages)})  ")

    # remove temporary directory
    tmp_dir.cleanup()

    return package_sets


def get_available_releases(print_result: bool = False) -> List[str]:
    """
    Return available MPSD software release versions.

    Example
    -------
    >>> get_available_releases()
    ["dev-23a"]

    Notes
    -----
    This needs to be updated when a new version (such as 23b) is released.
    """
    releases = ["dev-23a"]
    print_log = logging.getLogger("print")
    if print_result:
        print_log.info("Available MPSD software releases:")
        for release in releases:
            print_log.info(f"    {release}")
    return releases
