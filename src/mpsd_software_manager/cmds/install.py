"""Functions to install software."""
import logging
import sys
from pathlib import Path
from typing import List

from .prepare import prepare_environment
from mpsd_software_manager.utils.filesystem_utils import get_important_folders, os_chdir
from mpsd_software_manager.utils.logging import get_log_file_path
from mpsd_software_manager.utils.run import run


def install_environment(
    mpsd_release: str,
    package_sets: List[str],
    root_dir: Path,
    enable_build_cache: bool = False,
) -> None:
    """
    Install the specified MPSD release and package_sets.

    The function installs the package_set to the specified directory, using Spack.

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    package_sets : list of str
        A list of strings representing the package_sets to install
        (e.g., "foss2021a-mpi", "global_generic", "ALL").
    root_dir : pathlib.Path
        A Path object representing the path to the directory where
        the release and package_sets will be installed.
    enable_build_cache : bool, optional
        A boolean indicating whether to build the build cache
        when installing package_sets. Defaults to False.

    Raises
    ------
    ValueError
        If a requested package_set is not available in the specified release.

    Returns
    -------
    None
    """
    logging.info(
        f"Installing release {mpsd_release} with package_sets {package_sets} "
        f"to {root_dir}"
    )

    # Set required variables
    folders = get_important_folders(mpsd_release, root_dir)
    folders.package_set_dir.mkdir(parents=True, exist_ok=True)
    install_flags = []
    if not enable_build_cache:
        install_flags.append("-b")

    # run the prepare_environment function
    available_package_sets = prepare_environment(mpsd_release, root_dir)
    # Ensure that the requested package_sets are available in the release
    if package_sets == ["ALL"]:
        package_sets = available_package_sets
    elif package_sets == "NONE":
        # TODO: This if case is never reached, because of new argparse logic. Remove it.
        # No package_sets requested, so we only create the env and print the
        # list of available package_sets
        logging.warning(
            "No package_sets requested. Available package_sets for release "
            f"{mpsd_release} are: \n {available_package_sets}"
        )
        print_log = logging.getLogger("print")
        print_log.info(f"available_package_sets={available_package_sets}")
        return

    for package_set in package_sets:
        if package_set not in available_package_sets:
            msg = f"Package_Set '{package_set}' is not available"
            msg += f" in release {mpsd_release}. "
            msg += "Use 'available' command to see list of available package_sets."
            logging.error(msg)
            sys.exit(20)

    # Install the package_sets
    with os_chdir(folders.package_set_dir):
        # run spack_setup_script with the package_sets as arguments
        for package_set in package_sets:
            # Set the install log file name from create_log_file_names
            build_log_path = get_log_file_path(
                mpsd_release, "install", root_dir, package_set
            )

            # Log the build_log_path and the package_set_dir
            logging.info(
                f"Installing package_set {package_set} to {folders.release_base_dir}"
            )
            logging.info(f"> Logging installation of {package_set} at {build_log_path}")

            run(
                f"bash {folders.spack_setup_script} "
                f"{' '.join(install_flags)} {package_set} 2>&1 "
                f"| tee -a {build_log_path} ",
                shell=True,
                check=True,
            )
