"""Functions to initialise mpsd-software-root."""
import logging
import sys
from pathlib import Path

from mpsd_software_manager import __version__, config_vars
from mpsd_software_manager.utils.logging import (
    record_script_execution_summary,
    write_to_cmd_log,
)


def initialise_environment(root_dir: Path) -> None:
    """Initialize the software environment.

    This creates a hidden file ``.mpsd-software-root`` to tag the location for
    as the root of the installation. All compiled files, logs etc are written in
    or below this subdirectory.

    Parameters
    ----------
    root_dir : pathlib.Path
        A Path object pointing to the current directory where the script was called.

    """
    # check if the root_dir is not already initialized
    init_file = root_dir / config_vars.init_file
    if init_file.exists():
        logging.error(f"Directory {str(root_dir)} is already initialised.")
        sys.exit(30)
    else:
        # create the init file
        init_file.touch()
        # note the execution in the execution summary log
        # create the log file and fill it with the headers
        record_script_execution_summary(root_dir=root_dir)
        # record the msg in the log file
        init_log_msg = f"Initialising MPSD software instance at {root_dir}.\n"
        init_log_msg += f"MPSD Software manager version: {__version__}\n"
        write_to_cmd_log(
            root_dir=root_dir,
            msg=init_log_msg,
        )
