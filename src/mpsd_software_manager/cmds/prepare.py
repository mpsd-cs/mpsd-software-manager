"""Functions to prepare a new release."""
import logging
import os
import subprocess
from pathlib import Path
from typing import List, Tuple

from mpsd_software_manager import config_vars
from mpsd_software_manager.utils.filesystem_utils import (
    clone_repo,
    get_important_folders,
    os_chdir,
)
from mpsd_software_manager.utils.logging import write_to_cmd_log
from mpsd_software_manager.utils.run import run


def prepare_environment(
    mpsd_release: str, root_dir: Path, allow_unreleased_branch: bool = False
) -> List[str]:
    """
    Create the directory structure for the given MPSD release.

    It does the following steps:
    Clones the spack-environments repository.
    Determines the branch and commit hash of the spack-environments repository
    and the available package_sets.
    Logs the command usage.

    Parameters
    ----------
    mpsd_release : str
        The name of the MPSD release to prepare the environment for.
    root_dir : pathlib.Path
        The base directory to create the release folder and
        clone the spack-environments repository into.
    allow_unreleased_branch : bool, optional
        If True, allow the use of unreleased branches. Defaults to False.

    Returns
    -------
    available_package_sets : list
        A list of available package_sets for the given MPSD release.

    Example
    -------
    >>> prepare_environment('dev-23a', Path('.'))
    ['foss2021a-cuda-mpi',
     'foss2021a-mpi',
     'foss2021a-serial',
     'foss2022a-cuda-mpi',
     'foss2022a-mpi',
     'foss2022a-serial',
     'global',
     'global_generic']
    """
    # TODO review: - does this function need to return anything? If yes:
    # TODO review: - can we re-use get_available_package sets?

    logging.info(f"Preparing mpsd_release={mpsd_release}")

    # Creates the directory structure for the specified release and clone the
    # Spack environments repository if it doesn't exist:

    # Create the directory structure for the release
    folders = get_important_folders(mpsd_release, root_dir)
    folders.release_base_dir.mkdir(parents=True, exist_ok=True)
    if folders.repo_path.exists():
        logging.debug(f"directory {folders.repo_path} exists already, will update")
        with os_chdir(folders.repo_path):
            run(["git", "pull", "-v"], capture_output=True)
    else:
        repo_url = config_vars.spack_environments_repo
        logging.info(f"cloning repository {folders.repo_path} from {repo_url}")
        spe_branch = (
            mpsd_release if allow_unreleased_branch else f"releases/{mpsd_release}"
        )
        clone_repo(folders.repo_path, repo_url, branch=spe_branch)

    logging.getLogger("print").info(
        f"Release {mpsd_release} is prepared in {folders.release_base_dir}"
    )

    spe_branch, spe_commit_hash, available_package_sets = get_release_info(
        mpsd_release, root_dir
    )
    write_to_cmd_log(
        root_dir=root_dir,
        msg=(
            f"Spack environments branch: {spe_branch} "
            f"(commit hash: {spe_commit_hash})\n"
        ),
    )
    return available_package_sets


def get_release_info(mpsd_release: str, root_dir: Path) -> Tuple[str, str, List[str]]:
    """
    Get information about the specified release.

    Get information about the specified release, such as the branch and commit hash
    of the Spack environments repository and the available package_sets.

    Parameters
    ----------
    mpsd_release : str
        The name of the release to get information for.
    root_dir : pathlib.Path
        The base directory where releases are stored.

    Returns
    -------
    spe_branch : str
        The name of the branch for the Spack environments repository.
    spe_commit_hash : str
        The commit hash for the Spack environments repository.
    available_package_sets : list
        A list of strings representing the available package_sets for the release.

    Raises
    ------
    FileNotFoundError
        If the release directory does not exist.
    """
    # TODO - review this function: can we re-use get_available_package_sets?

    # Get the info for release
    folders = get_important_folders(mpsd_release, root_dir)
    if not os.path.exists(folders.release_base_dir):
        logging.debug(
            f"get_release_info(mpsd_release={mpsd_release}, root_dir={root_dir})"
        )
        raise FileNotFoundError(
            f"{folders.release_base_dir} does not exist.\n"
            f"Hint: `prepare {mpsd_release}` may fix this."
        )
    with os_chdir(folders.release_base_dir), os_chdir("spack-environments"):
        # Get the branch and commit hash of the spack-environments repo
        spe_commit_hash = (
            run(["git", "rev-parse", "HEAD"], stdout=subprocess.PIPE, check=True)
            .stdout.decode()
            .strip()
        )
        spe_branch = (
            run(
                ["git", "rev-parse", "--abbrev-ref", "HEAD"],
                stdout=subprocess.PIPE,
                check=True,
            )
            .stdout.decode()
            .strip()
        )
        available_package_sets = os.listdir("toolchains")
    return spe_branch, spe_commit_hash, available_package_sets
