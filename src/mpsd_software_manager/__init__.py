"""mpsd-software: tool for installation of software as on MPSD HPC."""
import datetime

try:
    import importlib.metadata as importlib_metadata
except ImportError:
    import importlib_metadata

import sys
from collections import namedtuple
from pathlib import Path

__version__ = importlib_metadata.version("mpsd_software_manager")


ConfigVars = namedtuple(
    "ConfigVars",
    [
        "cmd_log_file",
        "metadata_tag_open",
        "metadata_tag_close",
        "spack_environments_repo",
        "init_file",
    ],
)

config_vars = ConfigVars(
    cmd_log_file="mpsd-software.log",
    metadata_tag_open="!<meta>",
    metadata_tag_close="</meta>!",
    spack_environments_repo="https://gitlab.gwdg.de/mpsd-cs/spack-environments.git",
    init_file=".mpsd-software-root",
)

command_name = Path(sys.argv[0]).name

call_date_iso = (
    datetime.datetime.now().replace(microsecond=0).isoformat().replace(":", "-")
)
