"""Utility to get the microarchitecture."""
import logging
import os
import subprocess
import sys

try:
    from functools import cache
except ImportError:
    from functools import lru_cache

    cache = lru_cache(maxsize=None)

from .run import run
from mpsd_software_manager import config_vars


@cache
def get_native_microarchitecture():
    """Return native microarchitecture.

    On MPSD machines, there should be an environment variable "microarch".
    We try to read that. If it fails, we use the 'archspec cpu' command.
    If that fails, we ask the user to install it.

    Returns
    -------
    microarch : str

    Example
    -------
    >>> get_native_microarchitecture()
    'haswell'
    """
    # attempt to get MICRO_ARCH from environment variable (should work on
    # MPSD_HPC and MPSD linux laptops). If not defined, return
    # "UNKNOWN_MICROARCH"
    microarch = os.environ.get("MPSD_MICROARCH", "UNKNOWN_MICROARCH")

    # if we have not found the microarchitecture environment variable,
    # try calling archspec
    if microarch == "UNKNOWN_MICROARCH":
        logging.debug(
            "Couldn't find MPSD_MICROARCH environment variable. Will try archspec."
        )
        try:
            process = run(
                ["archspec", "cpu"], stdout=subprocess.PIPE, universal_newlines=True
            )
        except FileNotFoundError as e:
            logging.debug(f"Call of 'archspec cpu' failed: e={e}")
            # Presumably 'archspec' is not installed.
            msg = "Please install archspec, for example via 'pipx install archspec'.\n"
            msg += "The command we need to execute is 'archspec cpu'.\n"
            msg += "Documentation of package: https://archspec.readthedocs.io/"

            logging.error(msg)
            sys.exit(10)
        else:  # we have found archspec and executed it
            if process.returncode == 0:  # sanity check
                microarch = process.stdout.strip()
                logging.debug(
                    f"Found microarchitecture from 'archspec cpu' to be '{microarch}'"
                )
                assert len(microarch) > 0  # sanity check
            else:
                raise ValueError(
                    f"Some error occurred when calling 'archspec cpu': process={process}"  # noqa: E501
                )

    # at this point, we have determined the microarchitecture
    log_metadata("microarchitecture", microarch)
    return microarch


# Martin: TODO this should go to the logging module;
# moving it seemed to cause some problems due to circular imports
# needs to be addressed in the next round of refactoring
def log_metadata(key: str, value: str) -> None:
    """Log metadata to the log file.

    This function logs metadata to the log file. The metadata is
    enclosed in a tag, so that it can be easily found in the log file.
    logging module is used to write the metadata to the log file.

    Parameters
    ----------
    key : str
        key of the metadata
    value : str
        value of the metadata
    returns : None
    """
    logging.info(
        f"{config_vars.metadata_tag_open}{key}:{value}{config_vars.metadata_tag_close}"
    )
