"""Utility to run spack commands."""

import subprocess
from pathlib import Path

from mpsd_software_manager.utils.run import run


def spack_run(
    spack_dir: Path,
    spack_cmd: str,
    tee_output: str = None,
    capture_output: bool = False,
) -> subprocess.CompletedProcess:
    """
    Run a spack command.

    Parameters
    ----------
    mpsd_release : str
        A string representing the MPSD release version.
    root_dir : pathlib.Path
        A Path object representing the path to the directory where
        the release and package_sets will be installed.
    spack_cmd : str
        A string representing the spack command to run.
    tee_output : str, optional
        A string representing the path to a file to write the output of the
        spack command to. Defaults to None.
    capture_output : bool, optional
        A boolean indicating whether to capture the output of the spack
        command. Defaults to False.

    Returns
    -------
    spack_run_output : subprocess.CompletedProcess
        A CompletedProcess object representing the output of the spack command.
    """
    # TODO: modify this after merge of MR 130
    spack_env = spack_dir / "share" / "spack" / "setup-env.sh"

    commands_to_execute = [
        f"export SPACK_ROOT={spack_dir}",  # need to set SPACK_ROOT in dash and sh
        f". {spack_env}",
        f"{spack_cmd}",
    ]
    if tee_output:
        return run(
            "(" + " && ".join(commands_to_execute) + f") 2>&1 |tee -a {tee_output}",
            shell=True,
            check=True,
            capture_output=capture_output,
        )
    else:
        return run(
            "(" + " && ".join(commands_to_execute) + ")",
            shell=True,
            check=True,
            capture_output=capture_output,
        )
