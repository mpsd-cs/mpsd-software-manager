"""Coloured output of spack spec."""
from rich import print as rprint


def pretty_print_spec(spec: str) -> None:
    """
    Print the specs with colours using rich.

    - packages in white (everything until first %)
    - compiler in green (everything between % and first+)
    - variants in cyan (everything that starts with +)
    - build_system in yellow (everything that starts with build_system=)
    - architecture in purple (everything that starts with arch=)
    """
    # Note that this implementation necessitates the definition of
    # flags in the order in which we ask spack to format the output
    # also for flags that need the same colour because they are
    # interchangeable (like `+` and `~`) we need to define them together
    colour_map = {
        "%": "green",
        "+": "cyan",
        "~": "cyan",
        "build_system=": "yellow",
        "libs=": "blue",
        "arch=": "purple",
    }

    prev_colour = ""

    for flag in colour_map:
        # If the flag is in the spec string,
        # replace it with: previous closing colour, new colour, flag
        if flag in spec and (
            colour_map[flag] not in prev_colour
        ):  # avoid duplicates for eg when having both ~ and +
            spec = spec.replace(flag, f"{prev_colour}[{colour_map[flag]}]{flag}", 1)
            prev_colour = f"[/{colour_map[flag]}]"  # for next iter

    # Add the final closing tag to the spec string
    spec += prev_colour
    rprint(spec)
