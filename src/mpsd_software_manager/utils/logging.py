"""Logging utilities."""
import datetime
import logging
import re
import sys
from pathlib import Path
from typing import Union

import rich
import rich.logging

from mpsd_software_manager import call_date_iso, command_name, config_vars
from mpsd_software_manager.utils.microarch import get_native_microarchitecture


def set_up_logging(loglevel="warning", file_path=None):
    """Set up logging.

    This function sets up the logging configuration for the script.
    It configures the log level, log format, and log handlers
    for both file and console(=shell) output.


    Parameters
    ----------
    loglevel : str or int
       Loglevels are:
         - warning (default): only print statements if something is unexpected
         - info (show more detailed progress)
         - debug (show very detailed output)
    file_path : str
         - filename to save logging messages into

    If loglevel is 'debug', save line numbers in log messages.

    Returns
    -------
    None.

    Logger instances are generally not passed around, but retrieved from the
    logging module as shown below (they are singletons).

    We provide two loggers:

    1. log = logging.getLogger('')

       This is the 'root' logger.

       Typical use:

       log.debug("...")
       log.info("...")
       log.warn("...")

       Equivalent to

       logging.debug("...")
       logging.info("...")

    2. print_log = logging.getlogger('print')

       This uses the logging module to issue the message, but prints without
       any further markup (i.e. no date, loglevel, line number, etc). Think
       PRINT via the LOGging module.

       We use this as a replacement for the print function (i.e. for messages
       that should not be affected by logging levels, and which should always
       be printed).

       Typical and intended use:

       print_log.info("Available package_sets are ...")

       The major difference from the normal print command is that the output
       will be send to the stdout (as for print) AND the file with name
       filename, so that these messages appear in the log file together with
       normal log output.

    """
    # convert loglevel string into loglevel as number
    log_level_numeric = getattr(logging, loglevel.upper(), logging.WARNING)
    if not isinstance(log_level_numeric, int):
        raise ValueError("Invalid log level: %s" % loglevel)

    # set up the main logger ("root" logger)
    logger = logging.getLogger("")
    # - "logger" logs everything
    # - we use loglevel at handler level to write everything to file
    # - and filter using  log_level_numeric (as the user provides) to
    #   send logging messages to the console
    logger.setLevel(0)

    # the handler determines where the logs go: stdout/file
    # We use 'rich' to provide a Handler:
    # https://rich.readthedocs.io/en/stable/logging.html
    shell_handler = rich.logging.RichHandler()
    # rich handler provides metadata automatically:
    logging_format = "%(message)s"
    # for shell output, only show time (not date and time)
    shell_formatter = logging.Formatter(logging_format, datefmt="[%X]")

    # here we hook everything together
    shell_handler.setFormatter(shell_formatter)
    # use the log_level_numeric to decide how much logging is sent to shell
    shell_handler.setLevel(log_level_numeric)

    # Here we set the handlers of the RootLogger to be just the one we want.
    # The reason is that the logging module will add a <StreamHandler <stderr>
    # (NOTSET)> handler if logging.info/logging.debug/... is used before we
    # come across this line. And we do not want that additional handler.
    logger.handlers = [shell_handler]

    # if filename provided, write log messages to that file, too.
    if file_path:
        file_handler = logging.FileHandler(file_path)
        # if we have a file, we write all information in there.
        # We could change the level, for example restrict to only DEBUG and above with
        # file_handler.setLevel(logging.DEBUG)
        file_logging_format = "%(asctime)s %(levelname)7s %(lineno)4d  |  %(message)s"
        file_formatter = logging.Formatter(file_logging_format, datefmt="[%X]")
        file_handler.setFormatter(file_formatter)
        logger.addHandler(file_handler)

    #
    # new logger for printing
    #
    print_log = logging.getLogger("print")
    print_log.setLevel(logging.INFO)
    print_log.propagate = False
    # create formatter 'empty' formatter
    formatter = logging.Formatter("%(message)s")

    # create, format and set handler for shell output
    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    print_log.handlers = [ch]

    # if filename provided, write output of print_log to that file, too
    if file_path:
        # create, format and add file handler
        fh = logging.FileHandler(file_path)
        fh.setFormatter(formatter)
        print_log.addHandler(fh)

    #
    # short message
    #
    logging.debug(
        f"Logging has been setup, loglevel={loglevel.upper()} "
        + f"file_path={file_path}"
    )


def get_log_file_path(
    mpsd_release: str, cmd: str, root_dir: Path, package_set: Union[str, None] = None
) -> Union[Path, None]:
    """Get log file path.

    This function creates the log file paths for either the installer or
    the build log files.

    If a package_set is given, then the build log file path is returned.
    if no package_set is given, then the installer log file path is returned.

    If the logs folder does not exist, then it is created.

    Parameters
    ----------
    mpsd_release : str
        MPSD software stack version
    cmd : str
        command to be executed
    root_dir : str
        root directory of the mpsd software stack
    package_set : str
        package_set name (only for build log file)

    Returns
    -------
    Path or None
        log file path
        installer_log_file_path or build_log_file_path depending on the
        parameters given.

    Examples
    --------
    # installer log file path for `mpsd-software install dev-23a foss2021a-mpi`
    >>> get_log_file_path(
    ...     "dev-23a",
    ...     "install",
    ...     Path(
    ...         "/tmp/root_dir"
    ...     ),
    ... )
    PosixPath('/tmp/root_dir/dev-23a/logs/dev-23a_zen3_2023-07-03T12-28-55_APEX_install.log')

    # build log file path for `mpsd-software install dev-23a foss2021a-mpi`
    >>> get_log_file_path(
    ...     "dev-23a",
    ...     "install",
    ...     Path(
    ...         "/tmp/root_dir"
    ...     ),
    ...     "foss2021a-mpi",
    ... )
    PosixPath('/tmp/root_dir/dev-23a/logs/dev-23a_zen3_2023-07-03T12-28-55_BUILD_foss2021a-mpi_install.log')

    # installer log file path for `mpsd-software status dev-23a`
    >>> get_log_file_path(
    ...     "dev-23a",
    ...     "status",
    ...     Path(
    ...         "/tmp/root_dir"
    ...     ),
    ... )
    PosixPath('/tmp/root_dir/dev-23a/logs/dev-23a_zen3_2023-07-03T12-28-55_APEX_status.log')

    # build log file path for `mpsd-software status dev-23a` (no log file is created)
    >>> get_log_file_path(
    ...     "dev-23a",
    ...     "status",
    ...     Path(
    ...         "/tmp/root_dir"
    ...     ),
    ...     "foss2021a-mpi",
    ... )
    (None)
    """
    log_file_name = create_log_file_name(
        mpsd_release=mpsd_release,
        action=cmd,
        package_set=package_set,
    )
    log_folder = root_dir / mpsd_release / "logs"
    if log_file_name:
        # if the log_folder dosent exist, create it
        if not log_folder.exists():
            log_folder.mkdir(parents=True)
        return log_folder / log_file_name
    else:
        return None


def record_script_execution_summary(
    root_dir: Path, apex_log_file: Union[Path, None] = None
) -> None:
    """Log the command used to build the package_set.

    A date time header is added to the log file each time the script is called,
    following which the commands executed by the user is logged.
    The APEX log file is also logged if it is created.

    for example:

    ```

    2023-06-28T11:18:10.718020
    $ mpsd-software install dev-23a foss2021a-mpi
    ```

    Parameters
    ----------
    - root_dir : Path
        The path where the script is initialized.

    Returns
    -------
    - None

    """
    cmd_line = command_name + " " + " ".join(sys.argv[1:])
    date_time = datetime.datetime.now().replace(microsecond=0).isoformat()

    msg = f"\n {date_time}\n"
    msg += f"$ {cmd_line}\n"
    if apex_log_file:
        # apex_log_file is not known when `init` is called
        msg += f"> logging to {apex_log_file}\n"
    write_to_cmd_log(root_dir, msg)


def write_to_file(file_path: Path, content: str) -> None:
    """Write content to file.

    Parameters
    ----------
    file_path : Path
        The path to the file to write to.
    content : str
        The content to write to the file.

    Returns
    -------
    None

    """
    with open(file_path, "a") as f:
        f.write(content)


def write_to_cmd_log(root_dir: Path, msg: str) -> None:
    """Write message to command log.

    Parameters
    ----------
    root_dir : Path
        The path where the script is initialized.
    msg : str
        The message to write to the command log.

    Returns
    -------
    - None
    """
    cmd_log_file_path = root_dir / config_vars.cmd_log_file
    write_to_file(cmd_log_file_path, msg)


def create_log_file_name(
    mpsd_release: str,
    action: str,
    date: str = call_date_iso,
    package_set: Union[str, None] = None,
) -> Union[str, None]:
    """Create log file names.

    This function creates the log file names for either the installer or
    the build log files.

    If a package_set is given, then the build log file name is created.
    if no package_set is given, then the installer log file name is created.
    The installer log file hosts the logs of the installer script, while
    the build log file hosts the logs of the build process as generated by the
    spack_setup.sh script.

    Parameters
    ----------
    mpsd_release : str
        MPSD software stack version
    date : str
        date of the call ins iso format
    action : str
        action performed (install,remove,reinstall,prepare,status)
        only install and remove are valid for build log file.
    package_set : str or None
        package_set name (only for build log file)

    Returns
    -------
    str or None
        log file name
        installer_log_file_name or build_log_file_name depending on the
        parameters given.
        If the action is not one that changes the files on disk (info only actions)
        then None is returned.

    Examples
    --------
    # installer log file name for `mpsd-software install dev-23a foss2021a-mpi`
    >>> create_log_file_name(
    ...         "dev-23a",
    ...         "install",
    ...         "2023-07-03T12-27-52",
    ...     )
    'dev-23a_sandybridge_2023-07-03T12-27-52_APEX_install.log'

    # build log file name for `mpsd-software install dev-23a foss2021a-mpi`
    >>> create_log_file_name(
    ...     "dev-23a",
    ...     "install",
    ...     "2023-07-03T12-27-52",
    ...     "foss2021a-mpi",
    ... )
    'dev-23a_sandybridge_2023-07-03T12-27-52_BUILD_foss2021a-mpi_install.log'

    # installer log file name for `mpsd-software status dev-23a`
    >>> create_log_file_name(
    ...     "dev-23a",
    ...     "status",
    ...     "2023-07-03T12-27-52",
    ... )
    'dev-23a_sandybridge_2023-07-03T12-27-52_APEX_status.log'

    # build log file name for `mpsd-software status dev-23a` (no log file is created)
    >>> create_log_file_name(
    ...     "dev-23a",
    ...     "status",
    ...     "2023-07-03T12-27-52",
    ...     "foss2021a-mpi",
    ... )
    (None)
    """
    microarch = get_native_microarchitecture()
    if package_set:
        # if package_set is given, then  we build the build_log_file_name
        if action in ["install", "remove"]:
            log_file_name = (
                f"{mpsd_release}_{microarch}_{date}_BUILD_{package_set}_{action}.log"
            )
        else:
            return None
    else:
        # if package_set is not given, then we build the installer_log_file_name
        log_file_name = f"{mpsd_release}_{microarch}_{date}_APEX_{action}.log"

    return log_file_name


def read_metadata_from_logfile(logfile: Union[str, Path]) -> dict:
    """Read metadata from the log file.

    This function reads metadata from the log file. The metadata is
    enclosed in a tag, so that it can be easily found in the log file.

    Parameters
    ----------
    logfile : str or Path
        log file name
    returns : dict
        dictionary containing the metadata
    """
    with open(logfile) as f:
        log_text = f.read()
    # check for all data that matches the regex
    # metadata_tag_open {key}:{value} metadata_tag_close
    # and return a dictionary with all the matches
    return {
        match.group(1): match.group(2)
        for match in re.finditer(
            rf"{config_vars.metadata_tag_open}(\w+):(\w+){config_vars.metadata_tag_close}",
            log_text,
        )
    }
