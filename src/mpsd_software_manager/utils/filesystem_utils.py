"""Utilities related to working with files on disk."""
import logging
import os
import shutil
import sys
from collections import namedtuple

try:
    from functools import cache
except ImportError:
    from functools import lru_cache

    cache = lru_cache(maxsize=None)
from pathlib import Path

from .run import run
from mpsd_software_manager import config_vars
from mpsd_software_manager.utils.microarch import get_native_microarchitecture


def get_root_dir() -> Path:
    """Get the root directory of the installation.

    Look for the hidden file ``.mpsd-software-root``
    (defined in config_vars.init_file)
    in the current directory, or any parent directory.
    If found, return the path to the root directory
    of the MPSD software instance.
    If not found, exit with an error message.

    Returns
    -------
    root_dir : pathlib.Path
        A Path object pointing to the root directory of the installation.
        This folder contains the hidden file ``.mpsd-software-root``,
        ``mpsd_releases`` ( for eg ``dev-23a``) and ``mpsd-spack-cache``.


    """
    # check if the root_dir is not already initialized
    script_call_dir = Path.cwd()
    init_file = script_call_dir / config_vars.init_file
    if init_file.exists():
        return script_call_dir

    # if not, look for the init file in the parent directories
    for parent_folder in script_call_dir.parents:
        init_file = parent_folder / config_vars.init_file
        if init_file.exists():
            script_call_dir = parent_folder
            return script_call_dir

    # if not found in any parent directory, exit with an error message
    logging.debug(f"Directory {str(script_call_dir)} is not a MPSD software instance.")

    logging.error(
        "Could not find MPSD software instance "
        "in the current directory or any parent directory.\n\n"
        f"The current directory is {script_call_dir}.\n\n"
        "To initialise a MPSD software instance here, "
        "run 'mpsd-software init'.\n\n"
        f"To find the root directory of an existing MPSD software instance, look "
        f"for the directory containing '{config_vars.cmd_log_file}' "
        + f"and the hidden file '{config_vars.init_file}'."
    )
    sys.exit(40)


# Helper class to change directory via context manager
class os_chdir:
    """The os_chdir class is a context manager.

    It changes the current directory to a specified directory
    and returns to the original directory after execution.
    """

    def __init__(self, new_dir):
        """Initialize, save original directory."""
        self.new_dir = new_dir
        self.saved_dir = os.getcwd()

    def __enter__(self):
        """Go to target directory (main action for context)."""
        os.chdir(self.new_dir)

    def __exit__(self, exc_type, exc_val, exc_tb):
        """On exist we return to original directory."""
        os.chdir(self.saved_dir)


def clone_repo(
    target_path: Path, repo_url: str, branch=None, capture_output=True
) -> None:
    """Clone repo locally. Optionally checkout a branch.

    Parameters
    ----------
    target_path : Path
      Where to check the repository out to
    repo_url: str
      where to clone the git repository from
    branch: str (defaults to None)
      if provided, checkout this branch after cloning
    capture_output: bool (defaults to True)
      capture output, i.e. do not send it to stdout.
    """
    if not target_path.exists():
        target_path.mkdir()

    with os_chdir(target_path):
        run(
            ["git", "clone", repo_url, str(target_path)],
            check=True,
            capture_output=capture_output,
        )
    if branch:
        with os_chdir(target_path):
            # Git fetch and checkout the release branch and git pull
            # to be sure that the resulting repo is up to date
            run(["git", "fetch", "--all"], check=True, capture_output=capture_output)
            checkout_result = run(
                ["git", "checkout", branch], capture_output=capture_output
            )

            if checkout_result.returncode != 0:
                msg = f"Could not find the branch={branch}\n"
                branches_result = run(
                    ["git", "branch", "-a"], check=True, capture_output=True
                )
                branches_list = branches_result.stdout.decode().split("\n")
                removeprefix = lambda s, p: s[len(p) :] if s.startswith(p) else s  # noqa: E731
                branches_list = [
                    removeprefix(b.strip(), "remotes/origin/") for b in branches_list
                ]
                # show only release branches for clarity
                branches_list = [b for b in branches_list if b.startswith("releases/")]
                msg += f"Available branches are {branches_list}"
                logging.error(msg)
                # remove the mpsd_release folder which was created by the clone,
                # in order to prevent subsequent runs
                # of the non-existing mpsd_release from passing.
                shutil.rmtree(target_path)

                raise RuntimeError(msg, branches_result)
            else:
                run(["git", "pull"], check=True, capture_output=capture_output)


@cache
def get_important_folders(
    mpsd_release: str,
    root_dir: Path,
) -> namedtuple:
    """
    Return a named tuple of important folders for scripts as a Pathlib object.

    Parameters
    ----------
    mpsd_release : str
        The MPSD release version.
    root_dir : Path
        The root directory.

    Returns
    -------
    namedtuple
        A named tuple containing the important folders for scripts.
        currently: release_base_dir, package_set_dir, spack_dir,
        lmod_dir, spack_setup_script, logs_dir, repo_path

    Example
    -------
    >>> folders = get_important_folders("23b", Path("/opt/toolchain"))
    >>> print(folders.release_base_dir)
    Path("/opt/toolchain/23b")
    >>> print(folders.package_set_dir)
    Path("/opt/toolchain/23b/sandybridge")
    """
    Folders = namedtuple(
        "Folders",
        [
            "release_base_dir",
            "package_set_dir",
            "spack_dir",
            "lmod_dir",
            "spack_setup_script",
            "logs_dir",
            "repo_path",
        ],
    )

    release_base_dir = root_dir / mpsd_release
    microarch = get_native_microarchitecture()
    package_set_dir = release_base_dir / microarch  # Here lies spack and lmod folders
    spack_dir = package_set_dir / "spack"
    lmod_dir = package_set_dir / "lmod"
    spack_setup_script = release_base_dir / "spack-environments" / "spack_setup.sh"
    logs_dir = release_base_dir / "logs"
    repo_path = release_base_dir / "spack-environments"

    return Folders(
        release_base_dir=release_base_dir,
        package_set_dir=package_set_dir,
        spack_dir=spack_dir,
        lmod_dir=lmod_dir,
        spack_setup_script=spack_setup_script,
        logs_dir=logs_dir,
        repo_path=repo_path,
    )
