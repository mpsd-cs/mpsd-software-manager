import importlib
import os
import shutil
import subprocess
import sys
from pathlib import Path

import pytest

import mpsd_software_manager
from mpsd_software_manager.cmds import install


def test_install_environment_wrong_package_set(tmp_path):
    """Test exception is raised for non-existing package_set."""
    # exits with exit code 20 when wrong package_sets are provided
    with pytest.raises(SystemExit) as e:
        install.install_environment(
            mpsd_release="dev-23a",
            package_sets=["wrong-package_set"],
            root_dir=(tmp_path),
        )
    assert e.type == SystemExit
    assert e.value.code == 20


def test_install_environment_wrong_mpsd_release(tmp_path):
    """Test exception is raised for non-existing mpsd release."""
    # Expect an Exception when wrong mpsd_release is provided (part of
    # prepare_environment)
    with pytest.raises(RuntimeError):
        install.install_environment(
            mpsd_release="wrong-mpsd-release",
            package_sets=["foss2021a-mpi"],
            root_dir=(tmp_path),
        )


@pytest.mark.skipif(sys.platform == "darwin", reason="install not working on OSX")
def test_install_environment_zlib(create_mock_git_repository):
    """Test installation of package_set."""
    # Prepare a test installation of global generic
    # with only zlib to test the installation
    # This is a long test,
    # its handy to test this with print statements printed to
    # stdout, use:
    #   pytest -s
    # for this installation avoid tmp_path as
    # the length of the path becomes too long and spack complains
    root_dir = Path("/tmp/test_global_generic")
    if root_dir.exists():
        shutil.rmtree(root_dir)
    root_dir.mkdir(exist_ok=True, parents=True)
    mpsd_release_to_test = "dev-23a"
    package_set_to_test = "global_generic"
    cmd_log_file = mpsd_software_manager.config_vars.cmd_log_file
    microarch = mpsd_software_manager.utils.microarch.get_native_microarchitecture()
    folders = mpsd_software_manager.utils.filesystem_utils.get_important_folders(
        mpsd_release_to_test, root_dir
    )
    create_mock_git_repository(target_directory=root_dir, create_directory=False)
    install.prepare_environment(mpsd_release=mpsd_release_to_test, root_dir=(root_dir))
    # Patch the spack environments to create a fake global_generic
    # create a test package_set
    package_set_src_dir = folders.release_base_dir / "spack-environments" / "toolchains"
    # with install.os_chdir(package_set_src_dir):
    #     subprocess.run(
    #         "cp -r foss2021a-mpi fuss1999a",
    #         shell=True,
    #         stderr=subprocess.PIPE,
    #         stdout=subprocess.PIPE,
    #     )
    # add zlib as a spec to global_generic
    with open(
        package_set_src_dir / "global_generic" / "global_packages.list", "w"
    ) as f:
        f.write("zlib@1.2.13 \nzstd@1.5.2\n")

    # add zlib to whitelist of module creation file by replacing anaconda3%gcc@10.2.1
    # with zlib@1.2.13
    # in release_base_dir / "spack-environments/spack_overlay/etc/spack/modules.yaml"
    module_file = (
        folders.release_base_dir
        / "spack-environments/spack_overlay/etc/spack/modules.yaml"
    )
    with open(module_file) as f:
        lines = f.read().replace("anaconda3%gcc@10.2.1", "zlib@1.2.13")
    with open(module_file, "w") as f:
        f.write(lines)

    # Replace gcc@10.2.1 with gcc#13.1.1 or available system gcc for testing on laptop
    gcc_ver = (
        subprocess.run(
            ["gcc -dumpfullversion"],
            shell=True,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
        )
        .stdout.decode("utf-8")
        .strip()
    )
    assert len(gcc_ver) > 3, f"Couldn't find gcc gcc_ver={gcc_ver}"

    setup_file = folders.release_base_dir / "spack-environments/spack_setup.sh"
    with open(setup_file) as f:
        lines = f.read().replace(
            'system_compiler="gcc@10.2.1"', f'system_compiler="gcc@{gcc_ver}"'
        )
    with open(setup_file, "w") as f:
        f.write(lines)

    # install global_generic package_set
    mpsd_software_manager.utils.logging.set_up_logging(
        "WARNING",
        mpsd_software_manager.utils.logging.get_log_file_path(
            mpsd_release_to_test, "install", root_dir
        ),
    )
    install.install_environment(
        mpsd_release=mpsd_release_to_test,
        package_sets=[package_set_to_test],
        root_dir=root_dir,
        enable_build_cache=False,
    )
    # test that the build log is created correctly
    # check that a file with glob build_globale_generic_dev-23a*.log exists at
    # release_base_dir/microarch
    # print("Debug here ")
    # time.sleep(10)

    log_files = list(
        (folders.logs_dir).glob(f"{mpsd_release_to_test}_{microarch}_*_install.log")
    )
    assert len(log_files) == 2
    # take the most recent log as build log
    apex_log = sorted(log_files)[0]
    build_log = sorted(log_files)[1]
    assert "APEX" in str(apex_log)
    assert "BUILD" in str(build_log)
    # check that the build log contains statement ##### Installation finished
    with open(build_log) as f:
        lines = f.read()
        assert "##### Installation finished" in lines
    os.path.basename(build_log)

    # assert that APEX log file points to the build log file
    with open(apex_log) as f:
        lines = f.read()
        assert (
            f"> Logging installation of {package_set_to_test} at {build_log}" in lines
        )

    # assert that cmd log files exists
    assert os.path.exists(root_dir / cmd_log_file)

    # assert that the mpsd release and hash is written to the cmd log file
    os.path.basename(build_log)
    with open(root_dir / cmd_log_file) as f:
        lines = f.read()
        assert f"Spack environments branch: releases/{mpsd_release_to_test}" in lines
        # assert (
        #     f"> logging to {apex_log}" in lines
        # ) # TODO this has to be tested when main() called ie via CLI
    # assert that the module files are created correctly
    assert os.path.exists(folders.release_base_dir / microarch)
    assert os.path.exists(folders.release_base_dir / microarch / "lmod")
    # assert that lmod/module-index.yaml contains zlib
    with open(folders.release_base_dir / microarch / "lmod" / "module-index.yaml") as f:
        lines = f.read()
        assert "zlib" in lines

    # install again to ensure that
    # commands that skip creation of folders when
    # they are already present works as expected
    # reload the module to ensure that date changes

    # TODO Martin:
    # reloading is currently required, because the call time is saved as global
    # variable in the top-level __init__.py
    # This has to be changed
    importlib.reload(mpsd_software_manager)
    importlib.reload(mpsd_software_manager.utils.logging)

    mpsd_software_manager.utils.logging.set_up_logging(
        "WARNING",
        mpsd_software_manager.utils.logging.get_log_file_path(
            mpsd_release_to_test, "install", root_dir
        ),
    )
    install.install_environment(
        mpsd_release=mpsd_release_to_test,
        package_sets=[package_set_to_test],
        root_dir=root_dir,
        enable_build_cache=False,
    )
    build_log = list(
        (folders.release_base_dir / "logs").glob(
            f"{mpsd_release_to_test}_{microarch}_*_install.log"
        )
    )
    assert len(build_log) == 4

    # test that the removal now works
    # install.remove_environment(
    #     mpsd_release=mpsd_release_to_test,
    #     package_sets=[package_set_to_test],
    #     root_dir=root_dir,
    # )
    # # ensure that the module files are removed
