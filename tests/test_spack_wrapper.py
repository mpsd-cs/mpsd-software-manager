from mpsd_software_manager.utils.filesystem_utils import clone_repo
from mpsd_software_manager.utils.spack_wrapper import spack_run


def test_spack_run(tmp_path):
    """Test that spack_run works as expected."""
    # test that the init file is created as expected

    # clone the official spack repo in the tmp_path
    spack_dir = tmp_path / "spack"
    clone_repo(spack_dir, "https://github.com/spack/spack.git")

    test_file = str(spack_dir / "octopus_info.txt")
    spack_run(spack_dir, "spack info octopus", tee_output=test_file)
    with open(test_file) as f:
        assert "octopus-code.org" in f.read()
