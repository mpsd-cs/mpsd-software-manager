import pytest

import mpsd_software_manager
from mpsd_software_manager.cmds import init


def test_initialise_environment(tmp_path):
    """Test that init_file is created as expected."""
    # test that the init file is created as expected
    init.initialise_environment(tmp_path)
    init_file = tmp_path / mpsd_software_manager.config_vars.init_file

    assert init_file.exists()
    # ensure "Initialising MPSD software ..." is in the log file
    log_file = tmp_path / mpsd_software_manager.config_vars.cmd_log_file
    with open(log_file) as f:
        assert (f"Initialising MPSD software instance at {tmp_path}") in f.read()

    # test that calling again results in warning and exit code 30
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        init.initialise_environment(tmp_path)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 30
