import os
import subprocess

import pytest

from mpsd_software_manager.cmds import prepare
from mpsd_software_manager.utils.filesystem_utils import get_important_folders


def test_prepare_environment(tmp_path, create_mock_git_repository):
    """Simulate running preparation of environment.

    Simulate running ./install-software-environment.py --release dev-23a \
      --target-directory /tmp/test_prepare_env
    prepare_env is run when cmd is not specified, we can test cmd='prepare'
    and cmd=None to check both cases
    """
    root_dir = tmp_path / "mpsd_opt" / "linux_debian_11"
    spack_environments = "spack-environments"
    mpsd_release_to_test = "dev-23a"
    folders = get_important_folders(mpsd_release_to_test, root_dir)
    # check that the test directory does not exist
    assert not root_dir.exists()

    # prepare_environment expects to be executed in git repository
    # (mpsd-software-environments). It queries the commit on which we are to
    # log that information. For this to work, we need to execute the command
    # within a directory tree that has a git repository at the same or high
    # level. Let's create one:
    create_mock_git_repository(root_dir)

    # now call the function we want to test
    result = prepare.prepare_environment(
        mpsd_release=mpsd_release_to_test, root_dir=root_dir
    )

    # check if the directory now is created
    assert folders.release_base_dir.exists()
    # check for spack-environments directory
    assert spack_environments in os.listdir(folders.release_base_dir)

    # check if the git branch is correctly checked out. We expect output such as
    # git_branch_stdout = '* dev-23a\n  develop\n'
    # The entry with the '* ' prefix is the active branch.
    git_branch_output_raw = subprocess.run(
        f"cd {str(folders.repo_path)} && git branch",
        shell=True,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    git_branch_stdout = git_branch_output_raw.stdout.decode("utf-8")
    assert f"* releases/{mpsd_release_to_test}" in git_branch_stdout

    # check that result is a list and contains atleast ['global','foss2021a-mpi']
    assert isinstance(result, list)
    assert "global" in result
    assert "foss2021a-mpi" in result

    # Expect an Exception when wrong mpsd_release is provided
    with pytest.raises(RuntimeError):
        result = prepare.prepare_environment(
            mpsd_release="wrong-mpsd-release", root_dir=(root_dir)
        )

    # Expect an Exception when develop is prepared wihtout allow_unreleased_branch
    with pytest.raises(RuntimeError):
        result = prepare.prepare_environment(
            mpsd_release="develop", root_dir=(root_dir)
        )

    # Expect no Exception when develop is prepared with allow_unreleased_branch
    result = prepare.prepare_environment(
        mpsd_release="develop", root_dir=(root_dir), allow_unreleased_branch=True
    )
