from pathlib import Path

import pytest

import mpsd_software_manager
from .test_install import test_install_environment_zlib as install_environment_zlib
from mpsd_software_manager.cmds import remove
from mpsd_software_manager.utils.spack_wrapper import spack_run


def test_remove_environment(tmp_path, mocker, create_fake_environment):
    """Test that the remove_environment function works as expected."""

    release_to_test = "dev-23a"

    # check exit 50 when no package_sets are provided
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        remove.remove_environment("dev-23a", tmp_path)
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 50

    # Test case2  - remove entire release
    ## exit with 60 when force is not set
    ### patch the input function to return 'n'
    mocker.patch("builtins.input", return_value="n")
    with pytest.raises(SystemExit) as pytest_wrapped_e:
        remove.remove_environment(release_to_test, tmp_path, "ALL")
    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 60
    ### patch the input function to return 'y'
    mocker.patch("builtins.input", return_value="y")
    # check that the release directory is removed and logs are kept
    # create a release directory
    create_fake_environment(tmp_path, release_to_test)
    folders = mpsd_software_manager.utils.filesystem_utils.get_important_folders(
        release_to_test, tmp_path
    )

    toolchain_map = mpsd_software_manager.cmds.status.environment_status(
        release_to_test, tmp_path
    )
    assert toolchain_map is not None
    remove.remove_environment(release_to_test, tmp_path, "ALL")

    toolchain_map = mpsd_software_manager.cmds.status.environment_status(
        release_to_test, tmp_path
    )
    # check that no toolchain remains
    assert toolchain_map is None
    # check that the release directory is empty
    assert len(list(folders.package_set_dir.iterdir())) == 0
    # check that the logs directory is non-empty
    list_of_logs = list(folders.logs_dir.iterdir())
    assert len(list_of_logs) == 2  # APEX + remove_build_log
    # check that one of the log has 'remove.log' as part of the name
    assert "BUILD_ALL_remove.log" in ",".join([str(x) for x in list_of_logs])

    # Test case3  - remove specific package_sets
    # defined in test_remove_package_sets


def test_remove_package_sets(tmp_path, simple_toolchain, install_test_release):
    """Test removal of package_sets via spack."""

    release_to_test = "dev-23a"

    # Case1 - remove global / global_generic package_sets

    # Case2 - remove specific package_sets (toolchains)
    # Create a test install
    install_test_release(tmp_path, simple_toolchain)
    # check that the installation went through
    release_dir = tmp_path / release_to_test / remove.get_native_microarchitecture()
    assert len(list(release_dir.iterdir())) == 2  # spack and lmod
    # check that the two toolchains are installed
    environments_dir = release_dir / "spack" / "var" / "spack" / "environments"
    assert set([environment.name for environment in environments_dir.iterdir()]) == set(
        ["toolchain1", "toolchain2"]
    )
    # check that the two toolchains have the "handmade" module files
    toolchains_list = list(
        mpsd_software_manager.cmds.status.environment_status(
            release_to_test, tmp_path
        ).values()
    )[0]
    assert set(toolchains_list) == set(["toolchain1", "toolchain2"])

    # remove toolchain2
    # toolchain1 contains - zlib@1.2
    # toolchain2 contains - zlib@1.2 and zstd@1.5
    # we check that removing toolchain2 removes zstd@1.5 but NOT zlib@1.2
    # and the environment toolchain2 is also removed

    remove.remove_environment(
        mpsd_release=release_to_test,
        root_dir=tmp_path,
        package_sets=["toolchain2"],
        force_remove=True,
    )
    # now check that only "toolchain1" is installed in environments_dir
    assert set([environment.name for environment in environments_dir.iterdir()]) == set(
        ["toolchain1"]
    )

    # check that the only one toolchains has the "handmade" module files
    toolchains_list = list(
        mpsd_software_manager.cmds.status.environment_status(
            release_to_test, tmp_path
        ).values()
    )[0]
    assert set(toolchains_list) == set(["toolchain1"])

    # check that zlib@1.2 is still installed
    # spack location -i <package> exit 0 if installed and 1 if not installed
    spack_run(spack_dir=release_dir / "spack", spack_cmd="spack location -i zlib")
    # check that zstd@1.5 is not installed
    # we are here flipping the exit code to check that it is not installed
    spack_run(
        spack_dir=release_dir / "spack",
        spack_cmd="(spack location -i zstd && exit 1 || exit 0 )",
    )
    # check that the logs directory contains a build log for remove cmd
    # dev-23a_zen3_2023-08-11T15-55-54_BUILD_toolchain2_remove.log
    logs_dir = tmp_path / release_to_test / "logs"
    # remove_build_log is the last log file in the list
    remove_build_log = sorted(list(logs_dir.iterdir()))[-1]
    assert "toolchain2_remove.log" in remove_build_log.name
    with open(remove_build_log) as f:
        logs = f.read()
        assert "==> Will not uninstall zlib@" in logs
        assert "==> Successfully removed environment 'toolchain2'" in logs


def test_remove_global_package_sets():
    """Test removal of global package_sets via spack."""
    root_dir = Path("/tmp/test_global_generic")
    release_to_test = "dev-23a"
    if not root_dir.exists():
        # we need the sample spack instance with global_generic
        # this is already done in test_install_environment_zlib
        # so we just need to call it
        install_environment_zlib()
    # check that zlib and zstd are installed
    folders = mpsd_software_manager.utils.filesystem_utils.get_important_folders(
        release_to_test, root_dir
    )
    spack_env = folders.spack_dir / "share" / "spack" / "setup-env.sh"
    source_spack = f"export SPACK_ROOT={folders.spack_dir} && . {spack_env}"
    # check that zlib is installed
    # location commands exits with non zero if not installed thus
    # breaking failing test
    remove.run(f"{source_spack} && spack location -i zlib", shell=True, check=True)
    # check that zstd is installed
    remove.run(f"{source_spack} && spack location -i zstd", shell=True, check=True)

    # remove global_generic
    remove.remove_environment(
        mpsd_release=release_to_test,
        root_dir=root_dir,
        package_sets=["global_generic"],
        force_remove=True,
    )
    # check that zstd@1.5 is not installed
    # we are here flipping the exit code to check that it is not installed
    remove.run(
        f"{source_spack} && (spack location -i zstd && exit 1 || exit 0 )",
        shell=True,
        check=True,
    )
    # check that zlib is not installed
    remove.run(
        f"{source_spack} && (spack location -i zlib && exit 1 || exit 0 )",
        shell=True,
        check=True,
    )
    # check that the logs directory contains a build log for remove cmd
    # dev-23a_zen3_2023-08-11T15-55-54_BUILD_toolchain2_remove.log
    logs_dir = root_dir / release_to_test / "logs"
    # remove_build_log is the last log file in the list
    remove_build_log = sorted(list(logs_dir.iterdir()))[-1]
    assert "global_generic_remove.log" in remove_build_log.name
    with open(remove_build_log) as f:
        logs = f.read()
        assert "==> Successfully uninstalled zstd" in logs
        assert "==> Successfully uninstalled zlib" in logs
