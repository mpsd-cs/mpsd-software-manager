import os

import pytest

import mpsd_software_manager
from mpsd_software_manager.utils import filesystem_utils


def test_os_chdir(tmp_path):
    """Test the os_chdir context manager."""
    # create a temporary directory for testing
    temp_dir = tmp_path / "test_os_chdir"
    temp_dir.mkdir()

    # initial current working directory
    initial_cwd = os.getcwd()

    # change to the temporary directory using os_chdir
    with filesystem_utils.os_chdir(str(temp_dir)):
        assert os.getcwd() == str(temp_dir)

    # current working directory should be back to initial directory
    assert os.getcwd() == initial_cwd


def test_get_root_dir(tmp_path):
    """Test that the root directory is correct."""
    with filesystem_utils.os_chdir(tmp_path):
        # test that  function exists with error 40 if root dir doesn't exist
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            filesystem_utils.get_root_dir()
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 40

        # test that initialize_environment creates the root dir
        mpsd_software_manager.cmds.init.initialise_environment(tmp_path)
        root_dir = filesystem_utils.get_root_dir()
        assert root_dir == tmp_path

        # test that root_dir from parent is detected correctly
        sub_dir = tmp_path / "sub_dir"
        sub_dir.mkdir()
        with filesystem_utils.os_chdir(sub_dir):
            root_dir = filesystem_utils.get_root_dir()
            assert root_dir == tmp_path

        # test that initialising in a subdirectory makes it the root dir
        with filesystem_utils.os_chdir(sub_dir):
            mpsd_software_manager.cmds.init.initialise_environment(sub_dir)
            root_dir = filesystem_utils.get_root_dir()
            assert root_dir == sub_dir
