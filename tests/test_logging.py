import datetime
import logging
import os

import mpsd_software_manager
from mpsd_software_manager.utils import logging as software_manager_logging


def test_write_to_cmd_log(tmp_path):
    """Check that we write to the correct log file"""
    cmd_log_file = mpsd_software_manager.config_vars.cmd_log_file
    software_manager_logging.write_to_cmd_log(root_dir=tmp_path, msg="test_cmd")
    assert os.path.exists(tmp_path / cmd_log_file)
    with open(tmp_path / cmd_log_file) as f:
        assert "test_cmd" in f.read()


def test_record_script_execution_summary(tmp_path, create_mock_git_repository):
    """Check that cmd log is updated with header

    Check that logs/install-software-environment.log is updated when the module is run
    """
    cmd_log_file = mpsd_software_manager.config_vars.cmd_log_file

    root_dir = tmp_path / "test_prepare_env"
    script_version = mpsd_software_manager.__version__
    if os.path.exists(root_dir / cmd_log_file):
        initial_bytes = os.path.getsize(cmd_log_file)
    else:
        initial_bytes = 0

    # run the init functionality to check the creation of log file
    create_mock_git_repository(target_directory=root_dir, create_directory=True)
    mpsd_software_manager.cmds.init.initialise_environment(root_dir=(root_dir))

    # check that logs/install-software-environment.log is updated
    assert os.path.exists(root_dir / cmd_log_file)
    assert os.path.getsize(root_dir / cmd_log_file) > initial_bytes

    # Check that the log file has "Spack environments branch: dev-23a " in the last line
    with open(root_dir / cmd_log_file) as f:
        lines = f.readlines()
        assert f"Initialising MPSD software instance at {tmp_path}" in lines[-2]
        assert f"MPSD Software manager version: {script_version}" in lines[-1]


def test_metadata_logging(tmp_path):
    """Test that metadata is logged and read correctly."""
    # Test that the metadata is logged correctly
    filename = tmp_path / "test-metadata.log"
    print(f"Writing to {filename}")
    software_manager_logging.set_up_logging(loglevel="debug", file_path=filename)

    # our test data
    keys = ["important_key", "important_key2"]
    values = ["important_value", "important_value2"]

    expected_log_entries = []
    for key, value in zip(keys, values):
        mpsd_software_manager.utils.microarch.log_metadata(key, value)
        open_tag = mpsd_software_manager.config_vars.metadata_tag_open
        close_tag = mpsd_software_manager.config_vars.metadata_tag_close
        expected_log = f"{open_tag}{key}:{value}{close_tag}"
        expected_log_entries.append(expected_log)
        logging.info(f"Add some other info (after adding key={key})")
        logging.debug("Add some other info")
        logging.warning("Add some other info")

    # Check that relevant lines show up in the log file somewhere
    with open(filename) as f:
        logfile_content = f.read()
        for expected_log in expected_log_entries:
            assert expected_log in logfile_content

    # Test that the metadata is read correctly using our parser
    read_dict = software_manager_logging.read_metadata_from_logfile(
        tmp_path / "test-metadata.log"
    )

    # check all entries are in the file
    for key, value in zip(keys, values):
        assert read_dict[key] == value

    # check no additional entries are there
    assert len(read_dict) == len(keys)


def test_create_log_file_name():
    """Test that the log file names are created correctly."""
    create_log_file_name = software_manager_logging.create_log_file_name
    mpsd_release = "dev-23a"
    microarch = mpsd_software_manager.utils.microarch.get_native_microarchitecture()
    date = datetime.datetime.now().replace(microsecond=0).isoformat()
    action = "install"
    package_set = "foss2021a"
    # test build_log_file_name  generation
    build_log_file_name = create_log_file_name(
        mpsd_release=mpsd_release,
        date=date,
        action=action,
        package_set=package_set,
    )
    assert (
        build_log_file_name
        == f"{mpsd_release}_{microarch}_{date}_BUILD_{package_set}_{action}.log"
    )
    installer_log_file_name = create_log_file_name(
        mpsd_release=mpsd_release,
        date=date,
        action=action,
    )
    assert (
        installer_log_file_name
        == f"{mpsd_release}_{microarch}_{date}_APEX_{action}.log"
    )
    # test no build log file for incorrect action
    build_log_file_name = create_log_file_name(
        mpsd_release=mpsd_release,
        date=date,
        action="status",
        package_set=package_set,
    )
    assert build_log_file_name is None
