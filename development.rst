Developers Setup
================
This section is for developers wanting to contribute to the project.

Setup pre-commit hooks
----------------------
The project uses `pre-commit <https://pre-commit.com/>`__ to run some checks before committing code.
Install pre-commit by running the following command::

    pip install pre-commit

To setup the pre-commit hooks, run the following command from the root of the project::

    pre-commit install

Then every time you commit, pre-commit will run all checks defined in `.pre-commit-config.yaml`.
you can run the pre-commit checks manually by running::

    pre-commit run --all-files

Debugging exit codes
--------------------
Non zero exit codes are used to indicate that the program exited due to an error.
There are multiple ways to debug this. You could run the program with more verbose logging::

    mpsd-software -l debug ...

Here is a list of exit codes and what they mean:
+-----------+------------------------------------------+----------------------------------------------------------------------------------+
| Exit code | Reason                                   | Solution                                                                         |
+===========+==========================================+==================================================================================+
| 10        | Call of 'archspec cpu' failed            | Please install archspec, for example via 'pipx install archspec'                 |
| 20        | Requested package set is not available   | Use 'available' command to see list of available package_sets                    |
| 30        | Current directory is already initialised | Check if you are in the  right directory                                         |
| 40        | Current directory is not initialised     | Check if you are in the  right directory, if so use 'init' command to initialise |
| 50        | No package set is selected               | Please specify package_sets to remove, or 'ALL' to remove all package_sets       |
| 60        | User didnt confirm removing the release  | Please input 'y' if you are sure about removing the entire release               |
+-----------+------------------------------------------+----------------------------------------------------------------------------------+
