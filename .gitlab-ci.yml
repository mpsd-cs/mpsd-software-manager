image: python:latest

stages:
  - test

# prepare set up of latest python image to run style checks
.prepare_style: &prepare_style
  - cat /etc/issue
  - python3 -V
  - which python3
  - python3 -m venv ../venv
  - source ../venv/bin/activate
  - which python3
  - pwd
  - ls -l
  - pip install -U pip
  - pip --version
  - pip install .[dev]
  - pytest --version
  - cat /etc/issue


# prepare set up of Debian system to run py.test
.prepare_debian: &prepare_debian
  - echo "Execute this command before any 'script:' commands."
  - cat /etc/issue
  - pwd
  - ls -l

  - echo "Install Python3"
  - apt-get update
  - apt-get install -y python3 python3-venv
  - python3 -m venv --help
  - python3 -m venv venv
  - source venv/bin/activate
  - which python3
  - python3 --version

  - echo "Install Python dependencies for running the tests"
  - pip install -U pip
  - pip --version
  - pip install .[dev]
  - echo "Diagnostics - which versions are we using"
  - python3 --version
  - pytest --version

  - echo "Install additional packages we need to run spack-setup.sh"
  - apt-get install -y git rsync
  - echo "Install additional packages we need to run spack"
  # Taken from https://github.com/fangohr/oommf-in-spack/blob/main/Dockerfile
  - apt-get install -y --no-install-recommends
            autoconf
            build-essential
            ca-certificates
            coreutils
            curl
            environment-modules
            file
            gfortran
            git
            openssh-server
            unzip

  - export MPSD_MICROARCH=$(archspec cpu)
  - echo "Setting MPSD_MICROARCH variable to $MPSD_MICROARCH"
  - echo "Which version of Debian are we running?"
  - cat /etc/issue

.prepare_opensuse: &prepare_opensuse
  - echo -e "\e[0Ksection_start:`date +%s`:deps[collapsed=true]\r\e[0KInstall dependencies"
  - |
    zypper -n refresh
    zypper -n install --no-recommends python3 patterns-devel-base-devel_basis git gzip rsync
    python3 -m ensurepip
    pip3 install --upgrade pip setuptools
  - |
    python3 -m venv venv
    source venv/bin/activate
    type python3
    python3 --version
  - |
    pip3 install --upgrade pip setuptools
    pip3 install .[dev]
    pytest --version
  - echo -e "\e[0Ksection_end:`date +%s`:deps\r\e[0K"

  # https://gitlab.gwdg.de/mpsd-cs/spack-environments/-/merge_requests/56
  - echo -e "\e[0Ksection_start:`date +%s`:patch[collapsed=true]\r\e[0KBinary patching /bin/bash"
  - |
    cd "$(mktemp -d)"
    cp "$(readlink -e /bin/bash)" bash.patched
    printf '5.4' > dist_version
    dd if=dist_version of=bash.patched obs=1 seek=866776 conv=notrunc
    [ 5 -eq $(./bash.patched -c 'echo "${BASH_VERSINFO[0]}"') ] || { echo "Binary patching unsuccessful"; false; }
    mv bash.patched "$(readlink -e /bin/bash)"
    cd -
  - echo -e "\e[0Ksection_end:`date +%s`:patch\r\e[0K"

style:
  stage: test
  image: python:latest
  script:
    - *prepare_style
    - pre-commit run --all-files
  after_script:
    - |
      git diff --output=style.patch --exit-code || {
         cat style.patch
         false
      }
  artifacts:
    when: on_failure
    paths:
      - style.patch

test-bullseye:
  stage: test
  image: debian:bullseye-slim
  script:
    - *prepare_debian
    - pytest -v -l
test-bookworm:
  stage: test
  image: debian:bookworm-slim
  script:
    - *prepare_debian
    - pytest -v -l
test-opensuse:
  stage: test
  image: opensuse/leap:15.4
  before_script:
    *prepare_opensuse
  script:
    - pytest -v -l
