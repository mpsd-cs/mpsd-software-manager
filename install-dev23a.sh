#!/bin/bash
# Script to build all toolchains for this MPSD release ( 23a )
# Run this script inside the cloned folder for eg:
# mpsddeb@mpsd-hpc-ibm-022:/opt_mpsd/linux-debian11/mpsd-software-environments$ ./install-dev23a.sh

set -e
cd ..
mkdir -p dev-23a
cd dev-23a
# clone repo if it doesn't exist yet
[ -d 'spack-environments' ] || git clone git@gitlab.gwdg.de:mpsd-cs/spack-environments.git
pushd spack-environments
git checkout dev-23a
git pull
popd
mkdir -p sandybridge
cd sandybridge
../spack-environments/spack_setup.sh -b global
../spack-environments/spack_setup.sh foss2021a-serial
../spack-environments/spack_setup.sh foss2021a-mpi
../spack-environments/spack_setup.sh foss2021a-cuda-mpi


../spack-environments/spack_setup.sh foss2022a-serial
../spack-environments/spack_setup.sh foss2022a-mpi
../spack-environments/spack_setup.sh foss2022a-cuda-mpi
